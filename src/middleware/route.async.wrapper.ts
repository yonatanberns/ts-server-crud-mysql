export default (fn: (arg0: any, arg1: any, arg2: any) => Promise<any>) => (req:any , res :any, next:any ) => {
  fn(req, res, next).catch(next);
  // .catch((err)=> next(err))
};
