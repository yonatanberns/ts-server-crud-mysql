

const { NODE_ENV } = process.env;

export const error_handler =  (err: any, req: any, res: any, next: any) => {
    console.log(err);
    next(err)
}
export const error_handler2 =  (err: any, req: any, res: any, next: any) => {
    if(NODE_ENV !== 'production')res.status(500).json({status:err.message,stack:err.stack});
    else res.status(500).json({status:'internal server error...'});
}
export const not_found =  (req: any, res: any) => {
    console.log(`url: ${req.url} not found...`);
    res.status(404).json({status:`url: ${req.url} not found...`});
}


