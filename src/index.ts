import express from "express";
import morgan from 'morgan'
import cors from 'cors'
import user_router from './modules/user.router';
import {error_handler,error_handler2,not_found} from './middleware/errors.handler';   
import { env } from "./modules/user.validtion"

const { PORT,HOST} = process.env;

console.log('env params: ', PORT,HOST);
const host = env(HOST);


const app = express();

// middleware
app.use(cors());
app.use(morgan('dev'))

// routing
// app.use('/api/stories', story_router);

// users route
app.use('/api/users', user_router);

// central error handling
app.use(error_handler);
app.use(error_handler2);

//when no routes were matched...
app.use('*', (req, res)=> {
    res.send('invalid url..');
})

//start the express api server
;(async ()=> {
  //connect to mongo db
  // await connect_db(DB_URI);  
  await app.listen(Number(PORT),host);
  console.log(`api is live on`,` ✨ ⚡  http://${HOST}:${PORT} ✨ ⚡`);  
})().catch(console.log)



