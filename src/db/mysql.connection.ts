import mysql from 'mysql2/promise'
import { env } from '../modules/user.validtion'

const { HOST, USER_DATABASE, DATABASE, PORT_DATABASE, PASSWORD } = process.env; 
const host=env(HOST) , user=env(USER_DATABASE), database=env(DATABASE), port_database=env(PORT_DATABASE), password=env(PASSWORD) 
// console.log('in sql con:', host,USER_DATABASE, user, database, port_database, password);
 
  const getConnection = async()=> {

    const sqlConnection = await mysql.createConnection({host: host, user: user, database: database, port: Number(port_database), password: password});
    return sqlConnection;

  }

  export default getConnection();